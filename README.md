# Specs

Votre entreprise est spécialisée en cadeaux de Noël. Vous avez pour tâche d'implémenter la nouvelle interface de gestion de commandes.

Votre patron vous décrit ses attentes:

- Un client peut consulter la liste de produits en stock (nom, prix, quantité disponible) et faire une commande.
- Le responsable du magazin peut ajouter de nouveaux produits au stock, créer/modifier des packs cadeaux prédéfinis (ceux-ci seront stockés dans une base de donnée au format désiré: texte, xml, ... ).

Puis, le regard coquin, votre patron ajoute:

-- On pourrait supposer que le prix affiché au client dépendra de la quantité en stock.

Vous avez malheureusement travaillé trop longtemps dans cette boite pour avoir encore une quelconque morale. Vous décidez même de vous assurer que le module d'affichage de prix destinés aux clients soit extensible en prévision de nouvelles arnaques.