CC=g++
CFLAGS=-Wall -Wextra -Wpedantic -std=c++14

main_micro_e_commerce_app: main_micro_e_commerce_app.o
	$(CC) -o main_micro_e_commerce_app main_micro_e_commerce_app.o $(CFLAGS)

main_micro_e_commerce_app.o: src/main_micro_e_commerce_app.cpp
	$(CC) -o main_micro_e_commerce_app.o -c src/main_micro_e_commerce_app.cpp $(CFLAGS)

clean:
	rm -f *.o main_micro_e_commerce_app
