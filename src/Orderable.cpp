/*
 * Orderable.cpp
 *
 *  Created on: 17 janv. 2017
 *  Author: Florian Douay
 */

#include "Orderable.h"

int Orderable::idCounter = 0;

Orderable::Orderable(const string& description,
		  	  	     double basePrice) {
	this->id = ++idCounter;
	this->description = description;
	this->basePrice = basePrice;
	this->quantity = 0;
}

Orderable::~Orderable() {
	// TODO Auto-generated destructor stub
}
