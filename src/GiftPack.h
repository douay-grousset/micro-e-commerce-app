/*
 * GiftPack.h
 *
 *  Created on: 18 janv. 2017
 *  Author: Florian Douay
 */

#ifndef GIFTPACK_H_
#define GIFTPACK_H_

#include <vector>
#include <iostream>

#include "Orderable.h"

class GiftPack : public Orderable {

private:
	std::vector<int>* products;

public:
	GiftPack(const string& description,
 	         double basePrice);

	virtual ~GiftPack();

	std::vector<int>* getProducts() {
		return products;
	}

	void setProducts(std::vector<int>* products) {
		this->products = products;
	}

	ostream& operator<< (Orderable& obj) {
		return std::cout << obj.toString();
	}

	string toString();

};

#endif /* GIFTPACK_H_ */
