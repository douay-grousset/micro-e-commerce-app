/*
 * OrderableContainer.h
 *
 *  Created on: 18 janv. 2017
 *      Author: Florian Douay
 */

#ifndef ORDERABLECONTAINER_H_
#define ORDERABLECONTAINER_H_

#include <map>

#include "Orderable.h"

class OrderableContainer {

private:
	std::map<int,Orderable*>* orderables;

public:
	OrderableContainer();

	virtual ~OrderableContainer();

	bool addOrderable(Orderable* orderable);

	Orderable* get(int id);

	std::map<int,Orderable*>* getAll();

	bool deleteOrderable(int id);

};

#endif /* ORDERABLECONTAINER_H_ */
