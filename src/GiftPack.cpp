/*
 * GiftPack.cpp
 *
 *  Created on: 18 janv. 2017
 *  Author: Florian Douay
 */

#include "GiftPack.h"

#include <sstream>

GiftPack::GiftPack(const string& description,
	  	         double basePrice) : Orderable(description,basePrice), products(NULL) {
}

GiftPack::~GiftPack() {
	delete products;
}

string GiftPack::toString() {
	ostringstream oss;

	oss << "####" << "\n";
	oss << "gift pack : " << description << "\n";
	oss << "basePrice : " << basePrice << "\n";
	oss << "contains : " << "\n";

	for(uint i=0 ; i < products->size() ; ++i) {
	 oss << " * " << id << "\n";
	}
	oss << "####" << "\n";

	return oss.str();
}

