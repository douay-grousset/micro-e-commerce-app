/*
 * Orderable.h
 *
 *  Created on: 17 janv. 2017
 *  Author: Florian Douay
 */

#ifndef ORDERABLE_H_
#define ORDERABLE_H_

#include <string>

using namespace std;

class Orderable {

private:
	static int idCounter;

protected:
	int id;
	string description;
	double basePrice;
	int quantity;

public:
	Orderable(const string& description,
			  double basePrice);

	virtual ~Orderable();

	double getBasePrice() const {
		return basePrice;
	}

	void setBasePrice(double basePrice) {
		this->basePrice = basePrice;
	}

	const string& getDescription() const {
		return description;
	}

	void setDescription(const string& description) {
		this->description = description;
	}

	int getId() const {
		return id;
	}

	void setId(int id) {
		this->id = id;
	}

	int getQuantity() const {
		return quantity;
	}

	void setQuantity(int quantity) {
		this->quantity = quantity;
	}

	virtual ostream& operator<< (Orderable& obj) = 0;

	virtual string toString() = 0;
};

#endif /* ORDERABLE_H_ */
