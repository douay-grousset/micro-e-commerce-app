/*
 * OrderableContainer.cpp
 *
 *  Created on: 18 janv. 2017
 *      Author: Florian Douay
 */

#include "OrderableContainer.h"

using namespace std;

OrderableContainer::OrderableContainer() : orderables(new map<int,Orderable*>()) {

}

OrderableContainer::~OrderableContainer() {

	std::map<int,Orderable*>::iterator itEnd = orderables->end();

	for (std::map<int,Orderable*>::iterator it=orderables->begin(); it!=itEnd; ++it) {
		delete it->second;
	}
	delete orderables;
}

bool OrderableContainer::addOrderable(Orderable* orderable) {
	if(orderables->find(orderable->getId()) != orderables->end()) {
		//already exists
		return false;
	}
	orderables->insert(std::pair<int,Orderable*>(orderable->getId(),orderable));
	return true;
}

Orderable* OrderableContainer::get(int id) {

	std::map<int,Orderable*>::iterator it = orderables->find(id);

	if(it == orderables->end()) {
		return NULL;
	}
	return it->second;
}

std::map<int,Orderable*>* OrderableContainer::getAll() {
	return orderables;
}

bool OrderableContainer::deleteOrderable(int id) {

	std::map<int,Orderable*>::iterator it = orderables->find(id);

	if(it == orderables->end()) {
		//does not exist
		return false;
	}
	orderables->erase(it);
	return true;
}
