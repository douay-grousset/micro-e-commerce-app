/*
 * StockManager.h
 *
 *  Created on: 18 janv. 2017
 *      Author: Florian Douay
 */

#ifndef STOCKMANAGER_H_
#define STOCKMANAGER_H_

#include <string>
#include <map>

#include "OrderableContainer.h"

class StockManager {

private:

	static StockManager m_instance;

	OrderableContainer* orderableContainer;

	StockManager();

	virtual ~StockManager();

	StockManager (const StockManager&){ orderableContainer=NULL; }

	StockManager& operator= (const StockManager&) { return *this; }

public:

	static StockManager& getInstance();

	bool increaseStock(int id, int quantity);

	bool decreaseStock(int id, int quantity);

	bool createProduct(std::string description, double basePrice);

	bool createPack(std::string description, double basePrice, std::map<int,int> orderable);

	bool deleteOrderable(int id);

	Orderable* get(int id);

	std::map<int,Orderable*>* getAllOrderable();
};

#endif /* STOCKMANAGER_H_ */
