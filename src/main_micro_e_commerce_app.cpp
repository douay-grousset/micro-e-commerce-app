//============================================================================
// Name        : micro-e-commerce-app.cpp
// Author      : douay-grousset
// Version     :
// Copyright   : LGPLv3+
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <stdexcept>

#include "StockManager.h"
#include "OrderableContainer.h"
#include "Product.h"
#include "glob.h"

using namespace std;

void test1() {
	cout << "#### TEST 1 ####" << endl;
	OrderableContainer* oContainer = new OrderableContainer();

	cout << "initial number of orderable in container : " << oContainer->getAll()->size() << endl;
	cout << "**insertion of one product" << endl;
	oContainer->addOrderable(new Product("Ana", 55.2));
	cout << "current number of orderable in container : " << oContainer->getAll()->size() << endl;

	Orderable* o = oContainer->get(1);
	cout << "retrieve and print the product from container : " << endl;
	cout << "########" << endl;
	cout << "orderable " << o->getDescription() << endl;
	cout << "base price " << o->getBasePrice() << endl;
	cout << "id " << o->getId() << endl;
	cout << "quantity " << o->getQuantity() << endl;
	cout << "########" << endl;

	cout << "** delete product" << endl;
	oContainer->deleteOrderable(1);
	cout << "final number of orderable in container : " << oContainer->getAll()->size() << endl;

	delete oContainer;
}

void test2() {
	cout << "#### TEST 2 ####" << endl;
	cout << "initial number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;
	cout << "** insertion of one product" << endl;
	StockManager::getInstance().createProduct("Ana", 55.2);
	cout << "current number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;

	Orderable* o = StockManager::getInstance().get(1);
	cout << "retrieve and print the product from StockManager : " << endl;
	cout << "########" << endl;
	cout << "product " << o->getDescription() << endl;
	cout << "base price " << o->getBasePrice() << endl;
	cout << "id " << o->getId() << endl;
	cout << "quantity " << o->getQuantity() << endl;
	cout << "########" << endl;

	cout << "** increasing quantity (+10)" << endl;
	StockManager::getInstance().increaseStock(o->getId(),10);
	cout << "new quantity : " << o->getQuantity() << endl;

	cout << "** reduce quantity (-2) " << endl;
	StockManager::getInstance().decreaseStock(o->getId(),2);
	cout << "new quantity : " << o->getQuantity() << endl;

	cout << "** delete the product" << endl;
	StockManager::getInstance().deleteOrderable(1);
	cout << "final number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;
}

void test3() {
	cout << "#### TEST 3 ####" << endl;
	cout << "initial number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;
	cout << "** insertion of two products" << endl;
	StockManager::getInstance().createProduct("Ana", 55.2);
	StockManager::getInstance().createProduct("Ange", 82.1);
	cout << "current number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;

	cout << "** set quantity of the two protect to 2" << endl;
	StockManager::getInstance().increaseStock(1,2);
	StockManager::getInstance().increaseStock(2,2);
	cout << "** creation of one pack with the previous two products (2x2)" << endl;

	map<int,int> myMap = map<int,int>();
	myMap.insert(std::pair<int,int>(1,2));
	myMap.insert(std::pair<int,int>(2,2));

	StockManager::getInstance().createPack("mon pack 2x2", 120.5, myMap);

	cout << "pack quantity : " << StockManager::getInstance().get(3)->getQuantity() << endl;
	cout << "current number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;

	Orderable* o = StockManager::getInstance().get(3);
	cout << "retrieve and print the pack from StockManager : " << endl;
	//FIXME the print doesn't work for some reason
//	cout << *o << endl;

	cout << "** delete the product" << endl;
	StockManager::getInstance().deleteOrderable(3);
	cout << "final number of orderable in container : " << StockManager::getInstance().getAllOrderable()->size() << endl;
	cout << "final quantity of product 1 and 2 (must be 0, consumed by pack) : " << StockManager::getInstance().get(1)->getQuantity() << " and " << StockManager::getInstance().get(2)->getQuantity() << endl;
}

int main() {

	//do not lauch multiple tests (hardcoded id values <=> using auto increment static variable)

	//test1();

	//test2();

	test3();

	return 0;
}

