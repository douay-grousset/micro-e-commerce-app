/*
 * Product.cpp
 *
 *  Created on: 17 janv. 2017
 *  Author: Florian Douay
 */

#include "Product.h"

#include <sstream>

Product::Product(const string& description,
	  	         double basePrice) : Orderable(description,basePrice) {
}

Product::~Product() {
	// TODO Auto-generated destructor stub
}

string Product::toString() {
	ostringstream oss;

	oss << "####" << "\n";
	oss << "gift pack : " << description << "\n";
	oss << "basePrice : " << basePrice << "\n";
	oss << "contains : " << "\n";
	oss << "####" << "\n";

	return oss.str();
}
