/*
 * StockManager.cpp
 *
 *  Created on: 18 janv. 2017
 *      Author: Florian Douay
 */

#include "StockManager.h"
#include "Orderable.h"
#include "Product.h"
#include "GiftPack.h"

StockManager StockManager::m_instance=StockManager();

StockManager& StockManager::getInstance() {
    return m_instance;
}

StockManager::StockManager() : orderableContainer(new OrderableContainer()) {

}

StockManager::~StockManager() {
	delete orderableContainer;
}

bool StockManager::increaseStock(int id, int quantity) {

	Orderable* orderable = orderableContainer->get(id);

	if(orderable == NULL || quantity < 0) {
		return false;
	}
	orderable->setQuantity(orderable->getQuantity() + quantity);
	return true;
}

bool StockManager::decreaseStock(int id, int quantity) {

	Orderable* orderable = orderableContainer->get(id);

	if(orderable == NULL || quantity < 0) {
		return false;
	}
	orderable->setQuantity(orderable->getQuantity() - quantity);
	return true;
}

bool StockManager::createProduct(std::string description, double basePrice) {

	if(description.empty() || basePrice < 0) {
		return false;
	}
	Orderable* orderable = new Product(description,basePrice);
	return orderableContainer->addOrderable(orderable);
}

bool StockManager::createPack(std::string description, double basePrice, std::map<int,int> orderables) {
	if(description.empty() || basePrice < 0 || orderables.size() < 1) {
		return false;
	}

	//TODO move the check and consuming logic in increaseStock (for packs)
	//we want to check when we fill the stock, not we create the concept of a new pack

	Orderable* orderable = NULL;
	//map<id,quantity>
	std::map<int,int>::iterator itEnd = orderables.end();
	//checks if we have enough products to create this pack
	for (std::map<int,int>::iterator it=orderables.begin(); it!=itEnd; ++it) {
		orderable = orderableContainer->get(it->first);
		if(orderable == NULL || orderable->getQuantity() < it->second) {
			return false;
		}
	}

	//create giftpack
	GiftPack* giftPack = new GiftPack(description,basePrice);
	std::vector<int>* orderablesInPack = new vector<int>();

	itEnd = orderables.end();
	//decrement the number of products in stock due to the pack creation
	for (std::map<int,int>::iterator it=orderables.begin(); it!=itEnd; ++it) {

		Orderable* orderable = orderableContainer->get(it->first);
		orderable->setQuantity(orderable->getQuantity() - it->second);

		//add orderables's ids in pack's vector
		for(int i=0 ; i < it->second ; ++i) {
			orderablesInPack->push_back(it->first);
		}
	}
	giftPack->setProducts(orderablesInPack);

	return orderableContainer->addOrderable(giftPack);
}

bool StockManager::deleteOrderable(int id) {
	return orderableContainer->deleteOrderable(id);
}

Orderable* StockManager::get(int id) {
	return orderableContainer->get(id);
}

std::map<int,Orderable*>* StockManager::getAllOrderable() {
	return orderableContainer->getAll();
}

