/*
 * Product.h
 *
 *  Created on: 17 janv. 2017
 *  Author: Florian Douay
 */

#ifndef PRODUCT_H_
#define PRODUCT_H_

#include "Orderable.h"

#include <iostream>

class Product : public Orderable {
public:
	Product(const string& description,
 	        double basePrice);

	virtual ~Product();

	ostream& operator<< (Orderable& obj) {
		return std::cout << obj.toString();
	}

	string toString();
};

#endif /* PRODUCT_H_ */
